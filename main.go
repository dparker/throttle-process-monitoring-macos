package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"os/exec"
	"os/signal"
	"time"
)

func mainLoop() {
	file, err := os.Create(fmt.Sprintf("%vlog.txt", time.Now().Format(time.RFC3339)))

	if err != nil {
		log.Fatalln(err)
	}

	defer file.Close()

	reader := bufio.NewReader(os.Stdin)

	// Read and discard the first 2 lines
	reader.ReadString('\n')
	reader.ReadString('\n')

	for true {
		entry := ""

		for i := 0; i < 4; i++ {
			line, err := reader.ReadString('\n')
			if err != nil {
				log.Fatalln(err)
			}

			entry += line
		}

		// Get a snapshot of the current processes and CPU usage
		cmd := exec.Command("zsh", "-c", "ps -A -v -r | head -10")
		cmd.Env = append(os.Environ())
		processSnapshot, err := cmd.Output()

		if err != nil {
			log.Fatalln(err)
		}

		_, err = file.Write([]byte("\nNew thermal throttling event\n============================\n"))

		if err != nil {
			log.Fatalln(err)
		}

		_, err = file.Write([]byte(entry))

		if err != nil {
			log.Fatalln(err)
		}

		tempCmd, err := exec.Command("zsh", "-c", "osx-cpu-temp").Output()

		if err != nil {
			log.Fatalln(err)
		}

		_, err = file.Write([]byte(fmt.Sprintf("\n------------\nTEMP: %v------------\n\n", string(tempCmd))))

		if err != nil {
			log.Fatalln(err)
		}

		_, err = file.Write(processSnapshot)

		if err != nil {
			log.Fatalln(err)
		}
	}
}

func main() {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, os.Interrupt)

	go func() {
		<-sigs
		fmt.Printf("You pressed ctrl + C. User interrupted infinite loop.")
		os.Exit(0)
	}()

	mainLoop()
}
