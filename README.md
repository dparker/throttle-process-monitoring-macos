## What this script does
This script reacts to new stdin events: `pmset -g thermlog | ./main`.

Each time a new stdin event occurs from `pmset` it fetches some additional information:
1. CPU temp with `osx-cpu-temp`
1. The top ten processes that are running ordered by CPU usage with `ps -A -v -r | head -10`

It then logs the stdin lines + the above 2 additional bits of information to a log txt file in the working directory.

# How to build
```bash
go build main.go
```

# How to run
```bash
./run.sh
```
